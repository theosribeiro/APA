#include <stdio.h>
#include <stdlib.h>
//#include<vector>

char arqEntrada[] = "couting.txt"; 
char arqSaida[] = "couting_SAIDA.txt";

int tam = 0;
int qtdelinhas = 0;

void buildMaxHeap(int v[]);
void maxHeapify(int vetor[], int pos, int tamanhoDoVetor);
void swap(int v[], int j, int aposJ);

int QtdeLinhas(char *arq){
    FILE *arquivo = fopen(arq, "r");
    int caractere, existe_linhas = 0;
  
    while((caractere = fgetc(arquivo)) != EOF){
      existe_linhas = 3; 
    
      if(caractere == '\n'){ 
        qtdelinhas++;             
      } 
    }
    if(existe_linhas){
      qtdelinhas ++;
    }
	fclose(arquivo);
    //printf("O arquivo possui %d linhas.\n",qtdelinhas );
    return qtdelinhas;
}

void swap(int v[], int j, int aposJ) {
    int aux = v[j];
    v[j] = v[aposJ];
	v[aposJ] = aux;
}
    
void buildMaxHeap(int v[]) {
	int i;
	for ( i = tam / 2 - 1; i >= 0; i--) {
        maxHeapify(v, i, tam);
    }

}

void maxHeapify(int vetor[], int pos, int tamanhoDoVetor) {

    int max = 2 * pos + 1, right = max + 1;
    if (max < tamanhoDoVetor) {

        if (right < tamanhoDoVetor && vetor[max] < vetor[right]) {
            max = right;
        }

        if (vetor[max] > vetor[pos]) {
            swap(vetor, max, pos);
            maxHeapify(vetor, max, tamanhoDoVetor);
        }
    }
}

void HeapSort(int v[]) {
    buildMaxHeap(v);
    int n = tam,i;

    for (i = tam - 1; i > 0; i--) {
        swap(v, i, 0);
        maxHeapify(v, 0, --n);
    }
} 

void capturarArray(){
	tam = QtdeLinhas(arqEntrada);
    //printf("QTDE linhas : %d",qtdelinhas)

    //LER O ARQUIVO PARA PEGAR CADA NUMERO E SALVAR
    FILE *arq = fopen(arqEntrada, "r");
    //FILE *saida = fopen("saida.txt","wa"); //arqSaida
    FILE *saida = fopen(arqSaida,"w");
	//long double num[tam];
	int i,l;
	
	int num[tam];

	//PEGAR TODOS OS NUMEROS DO ARQUIVO E SALVAR NUM ARRAY
	for(i=0;i<tam;i++){
		
		fscanf(arq,"%d\n", &num[i]);
		printf("|%d| %d \n",i,num[i]);
		
	}
	//printf("\nPASSOU\n");
	//mergeSort(num,0,tam-1);
	HeapSort(num);
	//printf("\nPASSOU_MERGE\n");
	for(l=0;l<tam;l++){
		printf("|%d| %d \n",l,num[l]);
 		fprintf(saida, "%d\n", num[l]);
	}
}

int main(){
	capturarArray();
	return 0;
}