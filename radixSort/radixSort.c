#include <stdio.h>
#include <stdlib.h>
//#include<vector>

char arqEntrada[] = "couting.txt"; 
char arqSaida[] = "couting_SAIDA.txt";

int tam = 0;
int qtdelinhas = 0;

void buildMaxHeap(int v[]);
void maxHeapify(int vetor[], int pos, int tamanhoDoVetor);
void swap(int v[], int j, int aposJ);
void CountingSort(int array[], int indiceDaEsquerda, int indiceDaDireita);

int QtdeLinhas(char *arq){
    FILE *arquivo = fopen(arq, "r");
    int caractere, existe_linhas = 0;
  
    while((caractere = fgetc(arquivo)) != EOF){
      existe_linhas = 3; 
    
      if(caractere == '\n'){ 
        qtdelinhas++;             
      } 
    }
    if(existe_linhas){
      qtdelinhas ++;
    }
	fclose(arquivo);
    //printf("O arquivo possui %d linhas.\n",qtdelinhas );
    return qtdelinhas;
}
    
void radixSort(int vetor[], int tamanho) {
    int i;
    int *b;
    int maior = vetor[0];
    int exp = 1;

    b = (int *)calloc(tamanho, sizeof(int));

    for (i = 0; i < tamanho; i++) {
        if (vetor[i] > maior)
            maior = vetor[i];
    }

    while (maior/exp > 0) {
        int bucket[10] = { 0 };
        for (i = 0; i < tamanho; i++)
            bucket[(vetor[i] / exp) % 10]++;
        for (i = 1; i < 10; i++)
            bucket[i] += bucket[i - 1];
        for (i = tamanho - 1; i >= 0; i--)
            b[--bucket[(vetor[i] / exp) % 10]] = vetor[i];
        for (i = 0; i < tamanho; i++)
            vetor[i] = b[i];
        exp *= 10;
    }

    free(b);
}

void capturarArray(){
	tam = QtdeLinhas(arqEntrada);
    //printf("QTDE linhas : %d",qtdelinhas)

    //LER O ARQUIVO PARA PEGAR CADA NUMERO E SALVAR
    FILE *arq = fopen(arqEntrada, "r");
    //FILE *saida = fopen("saida.txt","wa"); //arqSaida
    FILE *saida = fopen(arqSaida,"w");
	//long double num[tam];
	int i,l;
	
	int num[tam];

	//PEGAR TODOS OS NUMEROS DO ARQUIVO E SALVAR NUM ARRAY
	for(i=0;i<tam;i++){
		
		fscanf(arq,"%d\n", &num[i]);
		printf("|%d| %d \n",i,num[i]);
		
	}
	printf("\nPASSOU\n");
	//mergeSort(num,0,tam-1);
	//HeapSort(num);
    //CountingSort(num,0,tam-1);
    radixSort(num,tam);
	//printf("\nPASSOU_MERGE\n");
	for(l=0;l<tam;l++){
		printf("|%d| %d \n",l,num[l]);
 		fprintf(saida, "%d\n", num[l]);
	}
    printf("\nFINISH\n");
}

int main(){
	capturarArray();
	return 0;
}