#include <stdio.h>
#include <stdlib.h>

//void mochila(int W, int wt[], int val[], int n);
void mochila();
int maiorValor(int a, int b){ 
  if (a>b){
    return a;
  }
  else{
    return b;   
  }
  
}
char arquivoentrada[] = "mochila01.txt";
char arquivosaida[] = "exemplo_SAIDA.txt";

int tam=0;

void mochila(int vetor[]);

int QtdeElementos(){
    FILE *arquivo = fopen(arquivoentrada, "r");
    int existe_linhas = 0, numCaracteres=0, numPalavras=0, numLinhas=0, comecouPalavra=0;
    int caractere;
   
    while ((caractere = fgetc(arquivo)) != EOF) {
        numCaracteres++;
        if  ((caractere!=' ') && (caractere!='\n') && (!comecouPalavra)) {
            comecouPalavra = 1;
        }
        if  (((caractere==' ') || (caractere == '\n')) && (comecouPalavra)) {
            comecouPalavra = 0;
            numPalavras++;
        }
    }           
    
  fclose(arquivo);
    return numPalavras;
}

void criaVetordeEntrada(){
  FILE *arquivo = fopen(arquivoentrada, "r");
  //FILE *arquivo2 = fopen(arquivosaida, "w");
  int i = 0;
  int aux;
  tam = QtdeElementos();
  int vetorentrada[tam];

  if(arquivo == NULL)
      printf("Erro, nao foi possivel abrir o arquivo\n");
  else{
    for (i = 0; i < tam; i++){ 
      fscanf(arquivo,"%d", &aux);
      vetorentrada[i] = aux; 
    }
  }

  //Kruskal(vetorentrada, tam);
  
  printf("\n Vetor de entrada: ");
  for(i = 0; i < tam; i++){
    printf("\n %d",vetorentrada[i]);
  }

  mochila(vetorentrada);
  
  //printf("\nvetor criado com sucesso");
  fclose(arquivo);
  //fclose(arquivo2);
}
 
void mochila(int vetor[]){
  int i, j;
  
  int numeroElementos = vetor[0], capacidadeMochila = vetor[1];

 /* scanf("%d", &numeroElementos);
  scanf("%d", &capacidadeMochila);*/

  printf("\n numElementos: %d\n capacidadeMochila: %d\n",numeroElementos,capacidadeMochila);

  int pesoElemento[numeroElementos];
  int valorElemento[numeroElementos];
  

  for(int i=0;i<numeroElementos;i++){
    pesoElemento[i]=0;
    valorElemento[i]=0;

  }
  int k=0;
  int g=0;
  for(i = 2; i < tam; i++){
    
    if(i % 2 == 0){ //CAPTURAR OS ELEMENTOS PARES, OU SEJA, OS PESOS DE CADA ELEMENTO
      pesoElemento[k] = vetor[i]; //i-2 para comecar pelo indice 0
      printf("\n%d  ",pesoElemento[k]);
      k++;
    }
    else{ //CAPTURAR OS ELEMENTOS IMPARES, OU SEJA, OS VALORES DE CADA ELEMENTO
      valorElemento[g] = vetor[i]; //i-2 para comecar pelo indice 0
      printf("%d \n",valorElemento[g]);
      g++;
    } 
  }
  printf("peso e valor dos elementos\n");
  for(int i=0;i<numeroElementos;i++){
    printf("%d - %d \n", pesoElemento[i],valorElemento[i]);
  }

  int matriz[numeroElementos+1][capacidadeMochila+1];

  printf("\nzerando matriz\n");

  for(int i=0;i<=numeroElementos;i++){
    printf("\n");
    for(int j=0;j<=capacidadeMochila;j++){
      matriz[i][j] = 0;
      printf("%d ", matriz[i][j]);
    }
  }
  
  for (i = 0; i <= numeroElementos; i++){
    printf("\n");
    for (j = 0; j <= capacidadeMochila; j++){
      printf(" %d %d ", i,j);

      printf("\n pelem: %d\n",pesoElemento[i]);
      printf("\n velem: %d\n",valorElemento[i]);

      if (i == 0 || j == 0){ 
        matriz[i][j] = 0;
      }   
      else if (pesoElemento[i-1] <= j){
        matriz[i][j] = maiorValor(valorElemento[i-1] + matriz[i-1][j-pesoElemento[i-1]], matriz[i-1][j]);
        printf("\npElem%d\n",pesoElemento[i-1]);
        printf("\nvElem%d\n",valorElemento[i-1]);           	
      }     
      else{
        matriz[i][j] = matriz[i-1][j];
      } 
	   	
    }
  }

  i--;
  j--;

  printf("\nValor Total: %d \n",matriz[numeroElementos][capacidadeMochila]);
  printf("\nElementos escolhidos: ");
	
  while(matriz[i][j]){
    if(matriz[i][j] == matriz[i-1][j]){
		  i--;
		}	
		else{
			printf(" [%d] ", i);
      i--;
			j = j - pesoElemento[i];
			printf(" - peso: | %d | ", pesoElemento[i]);
		}	
   }
}

int main(int argc, char** argv){
    
  //mochila();
  //int elem = QtdeElementos();
  //printf("qtde elementos: %d",elem);
  criaVetordeEntrada();
  return 0;
}
