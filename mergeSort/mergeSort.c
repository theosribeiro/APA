#include <stdio.h>
#include <stdlib.h>
//#include<vector>

char arqEntrada[] = "couting.txt"; 
char arqSaida[] = "couting_SAIDA.txt";

int tam = 0;
int qtdelinhas = 0;


int QtdeLinhas(char *arq){
    FILE *arquivo = fopen(arq, "r");
    int caractere, existe_linhas = 0;
  
    while((caractere = fgetc(arquivo)) != EOF){
      existe_linhas = 3; 
    
      if(caractere == '\n'){ 
        qtdelinhas++;             
      } 
    }

    if(existe_linhas){
      qtdelinhas ++;
    }
	
	fclose(arquivo);
    //printf("O arquivo possui %d linhas.\n",qtdelinhas );
    return qtdelinhas;
}

void merge(int vetor[], int comeco, int meio, int fim) {
    int com1 = comeco, com2 = meio+1, comAux = 0, tamanho = fim-comeco+1;
    int *vetAux;
    vetAux = (int*)malloc(tamanho * sizeof(int));

    while(com1<=meio && com2<=fim){
        if(vetor[com1] <= vetor[com2]){
            vetAux[comAux] = vetor[com1];
            com1++;
        }
        else{
            vetAux[comAux] = vetor[com2];
            com2++;
        }
        comAux++;
    }
    while(com1<=meio){  //Caso ainda haja elementos na primeira metade
        vetAux[comAux] = vetor[com1];
        comAux++;com1++;
    }
    while(com2<=fim){   //Caso ainda haja elementos na segunda metade
        vetAux[comAux] = vetor[com2];
        comAux++;com2++;

    }
    for(comAux=comeco;comAux<=fim;comAux++){    //Move os elementos de volta para o vetor original
        vetor[comAux] = vetAux[comAux-comeco];
    }
    free(vetAux);
}

void mergeSort(int vetor[], int comeco, int fim){

	//MERGESORT	
    if (comeco < fim) {
        int meio = (fim+comeco)/2;
        mergeSort(vetor, comeco, meio);
        mergeSort(vetor, meio+1, fim);
        merge(vetor, comeco, meio, fim);
    }
}

void capturarArray(){
	tam = QtdeLinhas(arqEntrada);
    //printf("QTDE linhas : %d",qtdelinhas)

    //LER O ARQUIVO PARA PEGAR CADA NUMERO E SALVAR
    FILE *arq = fopen(arqEntrada, "r");
    //FILE *saida = fopen("saida.txt","wa"); //arqSaida
    FILE *saida = fopen(arqSaida,"w");
	//long double num[tam];
	int i,l;
	
	int num[tam];

	//PEGAR TODOS OS NUMEROS DO ARQUIVO E SALVAR NUM ARRAY
	for(i=0;i<tam;i++){
		
		fscanf(arq,"%d\n", &num[i]);
		printf("|%d| %d \n",i,num[i]);
		
	}
	//printf("\nPASSOU\n");
	mergeSort(num,0,tam-1);
	//printf("\nPASSOU_MERGE\n");
	for(l=0;l<tam;l++){
		printf("|%d| %d \n",l,num[l]);
 		fprintf(saida, "%d\n", num[l]);
	}
}

int main(){

	capturarArray();
	//mergeSort();

	return 0;
}