#include <stdio.h>
#include <stdlib.h>
//#include<vector>

char arqEntrada[] = "couting.txt"; 
char arqSaida[] = "couting_SAIDA.txt";

int tam = 0;
int qtdelinhas = 0;


int QtdeLinhas(char *arq){
    FILE *arquivo = fopen(arq, "r");
    int caractere, existe_linhas = 0;
  
    while((caractere = fgetc(arquivo)) != EOF){
      existe_linhas = 3; 
    
      if(caractere == '\n'){ 
        qtdelinhas++;             
      } 
    }

    if(existe_linhas){
      qtdelinhas ++;
    }
	
	fclose(arquivo);
    //printf("O arquivo possui %d linhas.\n",qtdelinhas );
    return qtdelinhas;
}

int partition(int a[], int ini, int fim) {
   int pivot, i, j, t;
   pivot = a[ini];
   i = ini; j = fim+1;
        
    while(1){ //pegar todos os elementos menores que o pivo e colocar antes dele e os maiores depois.
        do ++i; 
        while( a[i] <= pivot && i <= fim );
        do --j; 
        while( a[j] > pivot );
        if( i >= j ){
            break;
        }
        t = a[i]; 
        a[i] = a[j]; 
        a[j] = t;
   }
   t = a[ini]; 
   a[ini] = a[j]; 
   a[j] = t;
   
   return j;
}

void quickSort(int a[], int ini, int fim){
    int j;
    if(ini < fim){
        // dividir e conquistar
        j = partition(a, ini, fim);
        quickSort(a, ini, j-1);
        quickSort(a, j+1, fim);
   }
    
}

void capturarArray(){
	tam = QtdeLinhas(arqEntrada);
    //printf("QTDE linhas : %d",qtdelinhas)

    //LER O ARQUIVO PARA PEGAR CADA NUMERO E SALVAR
    FILE *arq = fopen(arqEntrada, "r");
    //FILE *saida = fopen("saida.txt","wa"); //arqSaida
    FILE *saida = fopen(arqSaida,"w");
	//long double num[tam];
	int i,l;
	
	int num[tam];

	//PEGAR TODOS OS NUMEROS DO ARQUIVO E SALVAR NUM ARRAY
	for(i=0;i<tam;i++){
		
		fscanf(arq,"%d\n", &num[i]);
		printf("|%d| %d \n",i,num[i]);
		
	}
	//printf("\nPASSOU\n");
	//mergeSort(num,0,tam-1);
    quickSort(num, 0, tam-1);
	printf("\nPASSOU_QUICKSORT\n");
	for(l=0;l<tam;l++){
		printf("|%d| %d \n",l,num[l]);
 		fprintf(saida, "%d\n", num[l]);
	}
}

int main(){

	capturarArray();

	return 0;
}